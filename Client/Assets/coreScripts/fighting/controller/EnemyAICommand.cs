﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.common;
using Assets.coreScripts.fighting.model;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.controller
{
    public class EnemyAICommand : EventCommand
    {
        [Inject]
        public FightingCommon common { get; set; }
        public override void Execute()
        {
            var data = (CustomAIEventData)evt;
            switch (data.ai)
            {
                case GameConfig.AIEventType.AI_LOOP:
                    dispatcher.Dispatch(GameConfig.RoleEvent.ENEMY_MOVE, common.RandomDirection(data.enemyFlyHRDir));
                    break;
                case GameConfig.AIEventType.AI_FORCE:
                    if (data.currentObj.transform.localPosition.y >= data.enemyMaxFly)
                    {
                        dispatcher.Dispatch(GameConfig.RoleEvent.EMEMY_CANCEL_MOVE, GameConfig.Direction.DOWN);
                    }
                    if (data.currentObj.transform.localPosition.y <= data.enemyMinFly)
                    {
                        dispatcher.Dispatch(GameConfig.RoleEvent.ENEMY_MOVE, GameConfig.Direction.UP);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
