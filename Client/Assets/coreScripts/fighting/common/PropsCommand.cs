﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.common;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.fighting.util;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.common
{
   public class PropsCommand:EventCommand
    {
        [Inject]
        public IGameTimer timer { get; set; }

        public bool IsPause { get; set; }


        [Inject]
        public IUnitAPI customAPI { get; set; }

        public override void Execute()
        {
            CustomPropsEventData data = evt as CustomPropsEventData;          

            //dispatcher.Dispatch(data)

                //                view.titleImg.sprite = customAPI.LoadSprite("paused");
                //    break;
                //case GameConfig.PropsState.WIN:
                //    view.titleImg.sprite = customAPI.LoadSprite("win");
                //    break;
                //case GameConfig.PropsState.LOSE:
                //    view.titleImg.sprite = customAPI.LoadSprite("lose");

            switch (data.propStatus)
            {
                case GameConfig.PropsState.PAUSE_RELEASE:
                    timer.Start();
                    data.currentObj.SetActive(false);
                    dispatcher.Dispatch(GameConfig.PropsState.PAUSE_RELEASE);
                    break;
                case GameConfig.PropsState.OnPause:
                    timer.Stop();
                    data.img.sprite = customAPI.LoadSprite("paused");
                    data.img.SetNativeSize();
                    data.currentObj.SetActive(true);
                    dispatcher.Dispatch(GameConfig.PropsState.OnPause);
                    break;
                case GameConfig.PropsState.RESTART:
                    //data.type = data.propStatus;
                    dispatcher.Dispatch(GameConfig.CoreEvent.GAME_RESTART);
                    data.currentObj.SetActive(false);
                    break;
                case GameConfig.PropsState.WIN:
                case GameConfig.PropsState.LOSE:
                    data.type = data.propStatus;
                    dispatcher.Dispatch(GameConfig.PropsState.OnPause);
                    dispatcher.Dispatch(data.propStatus, data);
                    break;
                default:
                    break;
            }
        }
    }
}
