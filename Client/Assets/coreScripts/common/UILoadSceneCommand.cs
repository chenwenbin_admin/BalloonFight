﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace Assets.coreScripts.common
{
    public class UILoadSceneCommand:EventCommand
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        [Inject(ContextKeys.CONTEXT)]
        public Context context { get; set; }

        public override void Execute()
        {
            //Context.firstContext.RemoveContext(context);
            GameObject.Destroy(contextView);
            Application.LoadLevel((int)evt.data);
        }
    }
}
