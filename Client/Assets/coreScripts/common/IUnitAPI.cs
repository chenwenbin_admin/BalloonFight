﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace Assets.coreScripts.common
{
    public interface IUnitAPI
    {
        void CreatImage(Sprite sprite,Transform t);

        Sprite LoadSprite(string spriteName);

        List<T> LoadConfig<T>() where T : class;
    }
}
